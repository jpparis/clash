#! /bin/bash

[ -z "${ES_ENDPOINT}" ] && echo "*** Error: missing ES_ENDPOINT in environment." >&2 && exit 1
[ -z "${ES_USER}" ]     && echo "*** Error: missing ES_USER in environment."     >&2 && exit 1
[ -z "${ES_PASSWORD}" ] && echo "*** Error: missing ES_PASSWORD in environment." >&2 && exit 1
g_es=${ES_ENDPOINT}
g_creds="-u ${ES_USER}:${ES_PASSWORD}"

# ROLES - debug
# https://www.elastic.co/guide/en/elasticsearch/reference/current/security-api-get-role.html
#curl -s -k ${g_creds} \
#     -X GET \
#     ${g_es}/_security/role | jq . > roles.json

# ROLES
#https://www.elastic.co/guide/en/elastic-stack-overview/current/defining-roles.html
#https://www.elastic.co/guide/en/elasticsearch/reference/current/security-api-put-role.html
g_temp=_tmp_role.json
cat <<EOF > ${g_temp}
{
  "cluster": ["all"],
  "indices":  [
    {
      "names": [ "*" ],
      "privileges": [ "read" ],
      "query": ""
    }
  ],
  "applications": [],
  "run_as": [],
  "metadata": {
  },
  "transient_metadata": {
    "enabled": true
  }
}
EOF

#cat ${g_temp} || \
curl -s -k  ${g_creds} \
     -X POST -H "Content-Type: application/json" --data-binary "@${g_temp}" \
     ${g_es}/_security/role/elastic_read_all
echo
rm -rf ${g_temp}

# USERS
# https://www.elastic.co/guide/en/elasticsearch/reference/current/security-api-put-user.html
g_temp=_tmp_user.json
for u in $(jq -r 'keys| .[]' ${ES_PASSWORDS_FILE-/dev/null})
do
  echo "--- $u"
  curl -s -k  ${g_creds} \
       -X DELETE \
       ${g_es}/_security/user/$u
  echo

  l_passwd=$(jq '.'$u'.passwd' ${ES_PASSWORDS_FILE-/dev/null} | sed -e 's/"//g')
  cat <<EOF > ${g_temp}
{
  "full_name" : "$u",
  "password" : "$l_passwd",
  "roles" : [ "kibana_user", "elastic_read_all" ]
}
EOF

  #cat ${g_temp} || \
  curl -s -k  ${g_creds} \
       -X POST -H "Content-Type: application/json" --data-binary "@${g_temp}" \
       ${g_es}/_security/user/$u
  echo
done

rm -rf ${g_temp}
