#! /bin/bash

g_scripts_dir=$(dirname $0)

mkdir -p ${g_scripts_dir}/../jh
mkdir -p ${g_scripts_dir}/../bli2/ids

g_urls=$(cat ${g_scripts_dir}/../jh/conso.html | grep href | grep database | grep item | grep ',' | \
      sed -e '
s/^ *<a href="//g
s/".*$//g
')

# TODO
g_items_urls=$(cat ${g_scripts_dir}/../jh/conso.html | grep href | grep database | grep items)

# ##### download or not #####
g_urls=

for c_url in ${g_urls}
do
  echo "--- ${c_url}"
  l_base=$(basename $(echo ${c_url} | sed -e 's/https://'))
  curl -L --output ${g_scripts_dir}/../jh/${l_base}.html ${c_url}
  sleep 1
done

for c_html in ${g_scripts_dir}/../jh/*.html
do
  l_base=$(basename ${c_html} .html)
  cat ${c_html} | grep href | grep database | grep item/ | grep -v ',' | \
      sed -e '
s#^ *<a href="https://wowclassic.judgehype.com/database/fr/item/##g
s#/.*$##g
' > ${g_scripts_dir}/../bli2/ids/${l_base}.id
done
