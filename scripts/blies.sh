#! /bin/bash

g_scripts_dir=$(dirname $0)

g_temp=_tmp.json
g_temp_trace=_trace.json

[ -z "${ES_ENDPOINT}" ] && echo "*** Error: missing ES_ENDPOINT in environment." >&2 && exit 1
[ -z "${ES_INDEX}" ]    && echo "*** Error: missing ES_INDEX in environment." >&2    && exit 1
if [[ "${ES_ENDPOINT}" =~ "localhost:" ]]
then
  g_creds=""
else
  [ -z "${ES_USER}" ]     && echo "*** Error: missing ES_USER in environment."     >&2 && exit 1
  [ -z "${ES_PASSWORD}" ] && echo "*** Error: missing ES_PASSWORD in environment." >&2 && exit 1
  g_creds="-u ${ES_USER}:${ES_PASSWORD}"
fi

l_files=${g_scripts_dir}/../bli2/conso/*.json

g_es=${ES_ENDPOINT}
g_index=blibli

if [ "$1" == "reset" ]
then
  curl -s -k ${g_creds} \
       -X DELETE \
       ${g_es}/${g_index} | jq .

  curl -s -k ${g_creds} \
       -X PUT -H "Content-Type: application/json" \
       -d '{
"settings" : {"index" : {"number_of_shards" : 3, "number_of_replicas" : 1} },
"mappings" : {"properties" : {"timestamp" : { "type" : "date"} } }
}' \
       ${g_es}/${g_index} | jq . || exit 1
fi

for d in ${g_scripts_dir}/../bli2/*
do
  echo "--- $d"
  for f in $d/*.json
  do
    if [ -f $f ]
    then
      echo '{"index":{"_type":"_doc"}}'
      cat $f
      echo
    fi
  done > ${g_temp}

  if [ ! -s ${g_temp} ]
  then
    echo "--- Warning: skipping empty data file"
  else
    curl -s -k  ${g_creds} \
         -X POST -H "Content-Type: application/x-ndjson" --data-binary "@${g_temp}" \
         ${g_es}/${g_index}/_bulk > ${g_temp_trace}
    if [[ $? != 0 ]]
    then
      echo "*** Error: curl returned non 0"
      exit 1
    fi
    if [[ "false" != $(jq .errors ${g_temp_trace}) ]]
    then
      echo "*** Error: json response contains errors. (${g_temp_trace})"
      exit 1
    fi
  fi
done

