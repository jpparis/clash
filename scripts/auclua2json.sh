#! /bin/bash

g_path=${CLASH_DATA-./data}
g_scripts_dir=$(dirname $0)

g_temp=_ljtmp.json
g_temp_trace=_trace.json

l_files=${g_path}/*.lua

for f in $1
do
  l_jsonfile=$(dirname $f)/$(basename $f .lua).json

  ( echo '{'; \
    cat $f | \
      tr '\n' ' ' | \
      sed -e 's/\[//g
s/\]//g
s/ =/:/g
s/}, -- \([0-9]\)/},/g
s/, -- \([0-9]\)/: \1,/g
s/\(AUCTIONATOR_[A-Z_2]*\)/, "\1"/g
s/ITEM_ID_VERSION:/, "ITEM_ID_VERSION":/g
s/{ 	{/{ "zzz": {/g
s/, }/}/g
s/, 	*}/}/g
s/} "A/}, "A/g
s/}, 	{/}, "zzz": {/g
s/true "A/true, "A/g
s/nil/null/g
s/^ , //
';\
    echo '}' ) | /usr/bin/tr "" "\n" > ${l_jsonfile}
done
