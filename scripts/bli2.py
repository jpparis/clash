#! /usr/local/bin/python3

import json
import os
import sys
from pathlib import Path


# read file
g_data_path=os.environ["BLI2_DATA"]
g_db_file=os.environ["BLI2_DB"]

class BlibliDb:
  def __init__(self):
    self.m_db = dict()
    self.m_dbErrors = list()

  def _walkJsonFiles(self):
    for c_dir in Path(g_data_path).iterdir():
      for c_json in [ _ for _ in Path(c_dir).iterdir() if _.suffix == ".json" ]:
        yield c_json

  def execute(self):
    self.m_count = 0
    self.m_countErr = 0
    self.m_countOk = 0
    for c_json in self._walkJsonFiles():
      self.m_count += 1
      with open(c_json) as w_in:
        l_json = json.load(w_in)
      if "id" in l_json:
        self.m_countOk += 1
        l_name = l_json.pop("name")

        if l_name not in self.m_db:
          self.m_db[l_name] = dict()
          self.m_db[l_name]["id"] = list()
        self.m_db[l_name]["id"].append(l_json.pop("id"))

        del(l_json["_links"])
        del(l_json["media"])
        del(l_json["item_class"]["key"])
        del(l_json["item_subclass"]["key"])
        self.m_db[l_name].update(l_json)
      elif "code" in l_json:
        self.m_countErr += 1
        self.m_dbErrors.append(c_json)
      else:
        print(json.dumps(l_json, indent=2))
        sys.exit(1)
    with open(g_db_file, "w") as w_out:
      l_json = json.dump(self.m_db, w_out)

#------------------------------------------------------------------------------
def main(p_argv = None):
  l_bli2 = BlibliDb()
  l_bli2.execute()

  print("--- counts;", l_bli2.m_count, l_bli2.m_countOk, l_bli2.m_countErr)
  print("--- errors;", len(l_bli2.m_dbErrors))
  print("--- db:", len(l_bli2.m_db))
  for c_s in [ "Elixir d'oeil de chat", "Texte du crépuscule décodé" ]:
    print("---", c_s, json.dumps(l_bli2.m_db[c_s], indent=2))

if __name__ == "__main__":
  main(sys.argv)



#  *** Texte du crépuscule décodé /Volumes/MacintoshHDD/Users/jpp/gitlab/clash/bli2/conso/20676.json
#*** Texte du crépuscule décodé /Volumes/MacintoshHDD/Users/jpp/gitlab/clash/bli2/conso/20677.json
#*** Texte du crépuscule décodé /Volumes/MacintoshHDD/Users/jpp/gitlab/clash/bli2/conso/20678.json
#*** Texte du crépuscule décodé /Volumes/MacintoshHDD/Users/jpp/gitlab/clash/bli2/conso/20679.json
