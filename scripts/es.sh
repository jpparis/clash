#! /bin/bash

g_path=${CLASH_DATA-./data}
g_scripts_dir=$(dirname $0)

g_temp=_tmp.json
g_temp_trace=_trace.json

[ -z "${ES_ENDPOINT}" ] && echo "*** Error: missing ES_ENDPOINT in environment." >&2 && exit 1
[ -z "${ES_INDEX}" ]    && echo "*** Error: missing ES_INDEX in environment." >&2    && exit 1
if [[ "${ES_ENDPOINT}" =~ "localhost:" ]]
then
  g_creds=""
else
  [ -z "${ES_USER}" ]     && echo "*** Error: missing ES_USER in environment."     >&2 && exit 1
  [ -z "${ES_PASSWORD}" ] && echo "*** Error: missing ES_PASSWORD in environment." >&2 && exit 1
  g_creds="-u ${ES_USER}:${ES_PASSWORD}"
fi

g_es=${ES_ENDPOINT}
g_index=${ES_INDEX}

if [ "$1" == "reset" ]
then
  curl -s -k ${g_creds} \
       -X DELETE \
       ${g_es}/${g_index} | jq .

  curl -s -k ${g_creds} \
       -X PUT -H "Content-Type: application/json" \
       -d '{
"settings" : {"index" : {"number_of_shards" : 3, "number_of_replicas" : 1} },
"mappings" : {"properties" : {"timestamp" : { "type" : "date"} } }
}' \
       ${g_es}/${g_index} | jq . || exit 1
  l_files=${g_path}/*.lua
else
  l_ts=$(curl -s -k ${g_creds} \
       -X GET -H "Content-Type: application/json" \
       -d '{
  "size": 0,
  "query": {
    "match_all": {}
  },
  "aggs": {
    "last": {
      "max": {
        "field": "timestamp"
      }
    }
  }
}' \
       ${g_es}/${g_index}/_search | jq '.aggregations.last.value')
  l_ts=$(( $l_ts / 1000))
  l_files=$(find ${g_path} -type f -newermt "$(date '+%Y-%m-%d %H:%M:%S' -d @${l_ts})" | grep -v '\.json$')
fi

for f in ${l_files}
do
  echo "--- $f"
  ${g_scripts_dir}/auclua2json.sh $f || exit 1
  l_jsonfile=$(dirname $f)/$(basename $f .lua).json
  ${g_scripts_dir}/auc2es.py ${l_jsonfile} > ${g_temp} || exit 1
  if [ ! -s ${g_temp} ]
  then
    echo "--- Warning: skipping empty data file"
    continue
  fi
  curl -s -k  ${g_creds} \
       -X POST -H "Content-Type: application/x-ndjson" --data-binary "@${g_temp}" \
       ${g_es}/${g_index}/_bulk > ${g_temp_trace}
  if [[ $? != 0 ]]
  then
    echo "*** Error: curl returned non 0"
    exit 1
  fi
  if [[ "false" != $(jq .errors ${g_temp_trace}) ]]
  then
     echo "*** Error: json response contains errors. (${g_temp_trace})"
     exit 1
  fi
done

