#! /bin/bash

g_script_dir=$(dirname $0)

g_ts=$(date +"%Y-%m-%dT%H-%M-%S_%s")

for c_account in ${WOW_ACCOUNTS}
do
  l_file="${WOW_CLASSIC_HOME}/WTF/Account/${c_account}/SavedVariables/Auctionator.lua"
  [ -f "${l_file}" ] && cp "${l_file}" ${g_script_dir}/../data/auctionator_${c_account}_${g_ts}.lua
done
