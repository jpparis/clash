#! /bin/bash

g_scripts_dir=$(dirname $0)

g_apibase="https://eu.api.blizzard.com/data/wow"
g_nsbase="classic-eu"
g_locale="fr_FR"

g_token=$(curl -s -u "${WOW_CLIENT_ID}:${WOW_CLIENT_SECRET}" -d grant_type=client_credentials https://eu.battle.net/oauth/token \
            | jq -r .access_token)
g_hauth="Authorization: Bearer $g_token"

for c_file in $(ls -r ${g_scripts_dir}/../bli2/ids/*.id)
do
  echo "--- file: ${c_file}"
  g_odir=${g_scripts_dir}/../bli2/$(basename ${c_file} .id)
  mkdir -p ${g_odir}

  l_done=0
  for c_item_id in $(cat ${c_file})
  do
    if [ ! -f ${g_odir}/${c_item_id}.json ]
    then
      l_done=1
      echo -n " ${c_item_id}"
      curl -s --output ${g_odir}/${c_item_id}.json -H "${g_hauth}" "${g_apibase}/item/${c_item_id}?namespace=static-${g_nsbase}&locale=${g_locale}"
    fi
  done
  if [ 1 == ${l_done} ]
  then
    sleep 5
  fi
  echo
done
