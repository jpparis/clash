#! /usr/local/bin/python3

import json
import os
import sys

# read file
g_path=os.environ["CLASH_DATA"]

class Auctionator:
  def __init__(self, p_db):
    self.m_pricedObjs = {}
    self.m_objs = p_db

  def _priceDatabase2Json(self, p_db, p_ts):
    l_result = []
    del(p_db["__dbversion"])

    for c_server_faction in p_db:
      l_server_faction = p_db[c_server_faction]
      l_server, l_faction = c_server_faction.split("_")

      if not l_server in self.m_pricedObjs:
        self.m_pricedObjs[l_server] = {}
      if not l_faction in self.m_pricedObjs[l_server]:
        self.m_pricedObjs[l_server][l_faction] = {}

      for c_obj_name in l_server_faction:
        l_obj = l_server_faction[c_obj_name]
        l_obj_data = {}
        l_min = 123456789000
        l_max = 0
        l_count = 0
        l_sum = 0

        for c_key in l_obj:
          l_obj_data[c_key] = l_obj[c_key]

          if c_key.startswith("H") or c_key.startswith("L"):
            l_count += 1
            l_sum += l_obj[c_key]
            if l_min > l_obj[c_key]:
              l_min = l_obj[c_key]
            if l_max < l_obj[c_key]:
              l_max = l_obj[c_key]
          elif "mr" == c_key or "sc" == c_key or "cc" == c_key or "id" == c_key:
            pass
          else:
            print("Error: unknown key", c_key, file=sys.stderr)
            sys.exit(1)

        if 0 == l_count:
          continue

        l_json = {}
        l_json["data"] = l_obj_data

        l_json["min_price"] = l_min
        l_json["max_price"] = l_max
        l_json["count"] = l_count
        l_json["avg_price"] = float(l_sum) / float(l_count)

        l_json["timestamp"] = p_ts
        l_json["server"] = l_server
        l_json["faction"] = l_faction
        l_json["name"] = c_obj_name
        if c_obj_name in self.m_objs: l_json["blizzard"] = self.m_objs[c_obj_name]

        self.m_pricedObjs[l_server][l_faction][c_obj_name] = l_json

  def _toEs(self):
    l_meta = dict()
    l_meta["index"] = dict()
    l_meta["index"]["_type"] = "_doc"
    l_metas = json.dumps(l_meta)

    for c_server in self.m_pricedObjs:
      l_serverObjs = self.m_pricedObjs[c_server]
      l_keys = set()
      for c_faction in l_serverObjs:
        l_keys |= set(l_serverObjs[c_faction].keys())
      for c_objName in l_keys:
        if "Alliance" in l_serverObjs and c_objName in l_serverObjs["Alliance"]:
          l_ally = l_serverObjs["Alliance"][c_objName]
          if "Horde" in l_serverObjs and c_objName in l_serverObjs["Horde"]:
            l_horde = l_serverObjs["Horde"][c_objName]
            # both
            l_ally["a_minus_h_min"] = l_ally["min_price"] - l_horde["min_price"]
            l_ally["a_minus_h_avg"] = l_ally["avg_price"] - l_horde["avg_price"]
            l_ally["a_minus_h_max"] = l_ally["max_price"] - l_horde["max_price"]
            l_ally["a_minus_h_min_percent"] = l_ally["a_minus_h_min"] / l_ally["min_price"]
            l_ally["a_minus_h_avg_percent"] = l_ally["a_minus_h_avg"] / l_ally["avg_price"]
            l_ally["a_minus_h_max_percent"] = l_ally["a_minus_h_max"] / l_ally["max_price"]
            l_horde["a_minus_h_min"] = l_ally["a_minus_h_min"]
            l_horde["a_minus_h_avg"] = l_ally["a_minus_h_avg"]
            l_horde["a_minus_h_max"] = l_ally["a_minus_h_max"]
            l_horde["a_minus_h_min_percent"] = l_ally["a_minus_h_min_percent"]
            l_horde["a_minus_h_avg_percent"] = l_ally["a_minus_h_avg_percent"]
            l_horde["a_minus_h_max_percent"] = l_ally["a_minus_h_max_percent"]

            print(l_metas)
            print(json.dumps(l_ally))
            print(l_metas)
            print(json.dumps(l_horde))
          else:
            # ally
            l_ally["a_minus_h_min"]   = - 2 ** 31
            l_ally["a_minus_h_avg"]  = - 2 ** 31
            l_ally["a_minus_h_max"]   = - 2 ** 31
            print(l_metas)
            print(json.dumps(l_ally))
        else:
          # horde
          l_horde = l_serverObjs["Horde"][c_objName]
          l_horde["a_minus_h_min"]  = 2 ** 31
          l_horde["a_minus_h_avg"] = 2 ** 31
          l_horde["a_minus_h_max"]  = 2 ** 31
          print(l_metas)
          print(json.dumps(l_horde))


#------------------------------------------------------------------------------
def main(p_argv = None):
  if 2 != len(p_argv):
    print("*** Error: usage", file=sys.stderr)
    sys.exit(1)

  l_blizzDbFilename = os.environ["BLI2_DB"]
  l_filename = p_argv[1]
  with open(l_filename, "r") as w_in:
    l_aucJson = json.load(w_in)
  with open(l_blizzDbFilename) as w_in:
    l_db = json.load(w_in)

  l_name = l_filename.split("/")[-1][:-5]
  l_ts = int(l_name.split("_")[-1])

  l_auc = Auctionator(l_db)
  l_auc._priceDatabase2Json(l_aucJson["AUCTIONATOR_PRICE_DATABASE"], l_ts*1000)
  l_auc._toEs()

if __name__ == "__main__":
  main(sys.argv)
